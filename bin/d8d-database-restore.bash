#!/bin/bash

# shellcheck disable=SC2059
if [ ! "${1}" ]; then
  echo "Missing argument: path to dump file to restore."
    exit 1
fi

drush sql-query --file="${PATH_PROJECT}/${1}"
