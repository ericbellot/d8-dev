#!/bin/bash

# shellcheck disable=SC2059

if [ "${HOST_SYSTEM}" = "linux" ]; then
  sudo chmod -R g-w ${PATH_SITES}/default
  sudo chmod -R g+w ${PATH_SITES}/default/files
fi
