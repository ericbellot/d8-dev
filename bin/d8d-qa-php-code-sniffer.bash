#!/bin/bash

# shellcheck disable=SC2059
# Use PHP Code Beautifier and Fixer (phpcbf) to fixe errors.
phpcbf --report=full --standard="${PATH_PROJECT}/ruleset.phpcs.xml" "${PATH_PUBLIC_HTTP}/modules/custom"

# Use PHP Code Sniffer to display errors that remain to be corrected.
phpcs --report=full --standard="${PATH_PROJECT}/ruleset.phpcs.xml" "${PATH_PUBLIC_HTTP}/modules/custom"
