#!/bin/bash

# shellcheck disable=SC2059
phpunit --configuration "${PATH_PROJECT}/phpunit.custom.xml" "${@}"
