#!/bin/bash

# shellcheck disable=SC2059
# Update translations (distants and locales).
drush  locale-check
drush  locale-update

# Import translations overriding core and contrib strings.
drush langimp --langcode=fr "${PATH_PROJECT}/translations/custom/override.fr.po"
