#!/bin/bash

# shellcheck disable=SC2059
mkdir -p "${PATH_PUBLIC_HTTP}/modules/custom" "${PATH_PUBLIC_HTTP}/themes/custom"
phpmd \
  "${PATH_PUBLIC_HTTP}/modules/custom,${PATH_PUBLIC_HTTP}/themes/custom" \
  ansi \
  "${PATH_PROJECT}/ruleset.phpmd.xml" \
  --suffixes ctp,engine,hphp,inc,install,module,php,php4,php5,phtml,profile,test,theme
