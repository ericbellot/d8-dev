#!/bin/bash

# shellcheck disable=SC2059
php "${PATH_PUBLIC_HTTP}/core/scripts/run-tests.sh" --url "https://${DOMAIN_NAME}" --dburl "mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@db/${MYSQL_DATABASE}" --sqlite "${PATH_PROJECT}/tmp/test.sqlite"  "${@}"
