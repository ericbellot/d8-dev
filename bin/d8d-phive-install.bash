#!/bin/bash

# shellcheck disable=SC2059
mkdir -p "${PATH_PROJECT}/devtools"

# This command embed GPG keys of grumphp, phpcpd, phpmd and phpunit.
phive install --force-accept-unsigned --trust-gpg-keys BB5F005D6FFDD89E,E82B2FB314E9906E,0F9684B8B16B7AB0,4AA394086372C20A,31C7E470E2138192
