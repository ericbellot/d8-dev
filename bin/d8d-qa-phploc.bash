#!/bin/bash

# shellcheck disable=SC2059
# Use PHP Copy Paste Detector to found duplicated code.
phploc \
   --names=*.ctp,*.engine,*.hphp,*.inc,*.install,*.module,*.php,*.php4,*.php5,*.phtml,*.profile,*.test,*.theme \
   --ansi \
  "${PATH_PUBLIC_HTTP}"


