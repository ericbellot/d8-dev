#!/bin/bash

# shellcheck disable=SC2059
TODAY=$(date +%F-%H-%M-%S)

drush sql-dump --gzip --result-file="${PATH_PROJECT}/dump/dump-${TODAY}.sql"
