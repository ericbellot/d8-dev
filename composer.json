{
    "autoload": {
        "psr-4": {
            "Drupal\\Core\\Composer\\": "src/core/lib/Drupal/Core/Composer"
        }
    },
    "config": {
        "bin-dir": "./vendor/bin",
        "discard-changes": true,
        "optimize-autoloader": true,
        "platform": {
            "php": "7.3"
        },
        "process-timeout": 1800,
        "sort-packages": true
    },
    "conflict": {
        "drupal/drupal": "*"
    },
    "description": "Project template for Drupal 8 projects with a relocated document root",
    "extra": {
        "composer-exit-on-patch-failure": true,
        "drupal-l10n": {
            "destination": "sites/default/files/translations",
            "languages": [
                "fr"
            ]
        },
        "drupal-scaffold": {
            "excludes": [
                ".ht.router.php",
                "robots.txt",
                "web.config",
                "update.php"
            ],
            "initial": {
                ".editorconfig": "../.editorconfig",
                ".gitattributes": "../.gitattributes"
            },
            "locations": {
                "web-root": "public/"
            },
            "omit-defaults": false
        },
        "enable-patching": true,
        "installer-paths": {
            "drush/Commands/contrib/{$name}": [
                "type:drupal-drush"
            ],
            "public/core": [
                "type:drupal-core"
            ],
            "public/libraries/{$name}": [
                "type:drupal-library"
            ],
            "public/modules/contrib/{$name}": [
                "type:drupal-module"
            ],
            "public/modules/custom/{$name}": [
                "type:drupal-custom-module"
            ],
            "public/profiles/contrib/{$name}": [
                "type:drupal-profile"
            ],
            "public/profiles/custom/{$name}": [
                "type:drupal-custom-profile"
            ],
            "public/themes/contrib/{$name}": [
                "type:drupal-theme"
            ],
            "public/themes/custom/{$name}": [
                "type:drupal-custom-theme"
            ]
        },
        "installer-types": [
            "npm-asset"
        ],
        "patchLevel": {
            "drupal/core": "-p2"
        },
        "patches-ignore": {
            "drupal/core": {
                "Use PHPUnit 6 for testing when PHP version \u003e= 7.2": "https://www.drupal.org/files/issues/2927806-94.patch"
            }
        }
    },
    "homepage": "https://gitlab.com/ericbellot/d8-dev",
    "license": "GPL-2.0-or-later",
    "minimum-stability": "dev",
    "name": "d8dev/drupal-8",
    "prefer-stable": true,
    "repositories": [
        {
            "options": {
                "ssl": {
                    "allow_self_signed": false,
                    "verify_peer": true
                }
            },
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        },
        {
            "type": "composer",
            "url": "https://asset-packagist.org"
        }
    ],
    "require": {
        "php": ">=7.3",
        "ext-SPL": "*",
        "ext-SimpleXML": "*",
        "ext-date": "*",
        "ext-dom": "*",
        "ext-filter": "*",
        "ext-gd": "*",
        "ext-hash": "*",
        "ext-json": "*",
        "ext-pcre": "*",
        "ext-pdo": "*",
        "ext-session": "*",
        "ext-tokenizer": "*",
        "ext-xml": "*",
        "composer/installers": "^1.7",
        "cweagans/composer-patches": "^1.6",
        "drupal-composer/drupal-l10n": "^1.0",
        "drupal/admin_toolbar": "^2.0",
        "drupal/better_exposed_filters": "^4.0@alpha",
        "drupal/config_ignore": "^2.2",
        "drupal/config_pages": "^2.6",
        "drupal/config_split": "^1.4",
        "drupal/console": "^1.9",
        "drupal/core-composer-scaffold": "^8.8",
        "drupal/core-recommended": "^8.9.1",
        "drupal/drush_language": "^1.0@RC",
        "drupal/elasticsearch_connector": "^7.0@alpha",
        "drupal/eu_cookie_compliance": "^1.2",
        "drupal/facets": "^1.4",
        "drupal/field_group": "^3.0",
        "drupal/health_check": "^1.2",
        "drupal/honeypot": "^1.29",
        "drupal/link_attributes": "^1.10",
        "drupal/maxlength": "^1.0@beta",
        "drupal/metatag": "^1.10",
        "drupal/metatag_routes": "^1.0",
        "drupal/pathauto": "^1.5",
        "drupal/redirect": "^1.4",
        "drupal/redis": "^1.4",
        "drupal/robotstxt": "^1.3",
        "drupal/search_api": "^1.15",
        "drupal/search_api_autocomplete": "^1.2",
        "drupal/simple_sitemap": "^3.5",
        "drupal/smart_trim": "^1.2",
        "drupal/swiftmailer": "^1.0@beta",
        "drupal/views_bulk_operations": "^3.3",
        "drush/drush": "^9.7",
        "oomphinc/composer-installers-extender": "^1.1",
        "symfony/polyfill-php72": "^1.0",
        "wikimedia/composer-merge-plugin": "^1.4",
        "zaporylie/composer-drupal-optimizations": "^1.1"
    },
    "require-dev": {
        "drupal/coder": "^8.3",
        "drupal/config_inspector": "1.*",
        "drupal/devel": "2.*",
        "drupal/devel_a11y": "1.*",
        "drupal/devel_php": "^1.1",
        "drupal/examples": "^1.0",
        "drupal/pistachio": "1.*",
        "drupal/renderviz": "1.*",
        "drupal/search_kint": "1.*",
        "drupal/speedboxes": "1.*",
        "drupal/stage_file_proxy": "^1.0",
        "friendsoftwig/twigcs": "^3.2",
        "kgaut/potx": "8.x-1.x-dev",
        "phpunit/phpunit": "^7",
        "symfony/phpunit-bridge": "^5.0"
    },
    "scripts": {
        "install-codestandards": [
            "Dealerdirect\\Composer\\Plugin\\Installers\\PHPCodeSniffer\\Plugin::run"
        ]
    },
    "support": {
        "docs": "https://gitlab.com/ericbellot/d8-dev"
    },
    "type": "project"
}
