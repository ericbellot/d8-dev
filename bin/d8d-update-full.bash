#!/bin/bash

# shellcheck disable=SC2059

# Switch site to maintenance mode.
drush state-set system.maintenance_mode 1

source "${PATH_PROJECT}/bin/d8d-update.bash"

source "${PATH_PROJECT}/bin/d8d-config-import.bash"

source "${PATH_PROJECT}/bin/d8d-translations-update.bash"

# Quit maintenance mode.
drush state-set system.maintenance_mode 0
