{
  "autoload": {
    "psr-4": {
      "Drupal\\Core\\Composer\\": "src/core/lib/Drupal/Core/Composer"
    }
  },
  "config": {
    "bin-dir": "./vendor/bin",
    "discard-changes": true,
    "optimize-autoloader": true,
    "platform": {
      "php": "7.0.33"
    },
    "process-timeout": 1800,
    "sort-packages": true
  },
  "conflict": {
    "drupal/drupal": "*"
  },
  "description": "Drupal installation.",
  "extra": {
    "composer-exit-on-patch-failure": true,
    "drupal-l10n": {
      "destination": "../translations/contrib",
      "languages": [
        "fr"
      ]
    },
    "drupal-scaffold": {
      "initial": {
        ".editorconfig": "../.editorconfig",
        ".gitattributes": "../.gitattributes"
      },
      "excludes": [
        ".ht.router.php",
        "robots.txt",
        "web.config",
        "update.php"
      ],
      "omit-defaults": false
    },
    "installer-paths": {
      "web/core": [
        "type:drupal-core"
      ],
      "web/libraries/{$name}": [
        "type:drupal-library",
        "type:npm-asset"
      ],
      "web/modules/contrib/{$name}": [
        "type:drupal-module"
      ],
      "web/modules/custom/{$name}": [
        "type:drupal-custom-module"
      ],
      "web/profiles/contrib/{$name}": [
        "type:drupal-profile"
      ],
      "web/profiles/custom/{$name}": [
        "type:drupal-custom-profile"
      ],
      "web/themes/contrib/{$name}": [
        "type:drupal-theme"
      ],
      "web/themes/custom/{$name}": [
        "type:drupal-custom-theme"
      ],
      "drush/Commands/{$name}": [
        "type:drupal-drush"
      ]
    },
    "installer-types": [
      "npm-asset"
    ],
    "patchLevel": {
      "drupal/core": "-p2"
    }
  },
  "license": "GPL-2.0-or-later",
  "minimum-stability": "dev",
  "name": "ericbellot/drupal-8",
  "prefer-stable": true,
  "repositories": {
    "drupal": {
      "type": "composer",
      "url": "https://packages.drupal.org/8",
      "options": {
        "ssl": {
          "verify_peer": true,
          "allow_self_signed": false
        }
      }
    },
    "asset-packagist": {
      "type": "composer",
      "url": "https://asset-packagist.org"
    }
  },
  "require": {
    "php": ">=7.0",
    "ext-SPL": "*",
    "ext-SimpleXML": "*",
    "ext-date": "*",
    "ext-dom": "*",
    "ext-filter": "*",
    "ext-gd": "*",
    "ext-hash": "*",
    "ext-json": "*",
    "ext-pcre": "*",
    "ext-pdo": "*",
    "ext-session": "*",
    "ext-tokenizer": "*",
    "ext-xml": "*",
    "composer/installers": "^1.2",
    "cweagans/composer-patches": "^1.6",
    "drupal-composer/drupal-scaffold": "2.*",
    "drupal/config_installer": "^1.8",
    "drupal/core": "8.0.0",
    "drush/drush": "^8.0"
  },
  "require-dev": {
    "composer/composer": "^1.9"
  },
  "type": "project"
}
