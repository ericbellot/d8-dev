#!/bin/bash

# shellcheck disable=SC2059
source "${PATH_PROJECT}/bin/d8d-database-restore.bash" $1

source "${PATH_PROJECT}/bin/d8d-update-full.bash"
