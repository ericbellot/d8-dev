<?php

$databases['default']['default'] = array (
  'database' => $_ENV['MYSQL_DATABASE'],
  'username' => $_ENV['MYSQL_USER'],
  'password' => $_ENV['MYSQL_PASSWORD'],
  'prefix' => '',
  'host' => 'db',
  'port' => '3306',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

$settings['config_sync_directory'] = '../config/sync';

$settings['trusted_host_patterns'] = [
 '^.+\.d8dev\.localhost$',
];
$settings['file_temp_path'] = '../tmp';

// Redis configuration.
$settings['redis.connection']['interface'] = 'PhpRedis';
$settings['cache']['default'] = 'cache.backend.redis';
$settings['redis.connection']['host'] = 'redis';
$settings['redis.connection']['port'] = 6379;
$settings['redis.connection']['base'] = 0;
$settings['redis.connection']['password'] = '';
$settings['container_yamls'][] = 'modules/contrib/redis/redis.services.yml';
$settings['container_yamls'][] = 'modules/contrib/redis/example.services.yml';
// Always set the fast backend for bootstrap, discover and config, otherwise
// this gets lost when redis is enabled.
// @see https://api.drupal.org/api/drupal/core%21core.api.php/group/cache/8.5.x
$settings['cache']['bins']['bootstrap'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['discovery'] = 'cache.backend.chainedfast';
$settings['cache']['bins']['config'] = 'cache.backend.chainedfast';
