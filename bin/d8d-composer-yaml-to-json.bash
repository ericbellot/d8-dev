#!/bin/bash

# shellcheck disable=SC2059
COMPOSER_JSON_FILE="${PATH_PROJECT}/composer.json"

# Read composer.yml file and convert to JSON with pretty output format.
[[ -f "${COMPOSER_JSON_FILE}" ]] || touch "${COMPOSER_JSON_FILE}"
yq read --tojson --prettyPrint "${PATH_PROJECT}/composer.yml" > "${COMPOSER_JSON_FILE}"
composer validate
