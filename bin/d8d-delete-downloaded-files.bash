#!/bin/bash
# shellcheck disable=SC2059
rm -rf "${PATH_PROJECT}/devtools" "${PATH_PROJECT}/vendor" "${PATH_PROJECT}/tmp/*" "${PATH_PUBLIC_HTTP}/core" "${PATH_PUBLIC_HTTP}/modules/contrib" "${PATH_PUBLIC_HTTP}/themes/contrib" "${PATH_PUBLIC_HTTP}/profiles/contrib"

