#!/bin/bash

# shellcheck disable=SC2059
source "${PATH_PROJECT}/bin/d8d-installation-before.bash"

drush site-install minimal --existing-config \
    --account-name=${ADMIN_ACCOUNT_NAME} \
    --account-pass=${ADMIN_ACCOUNT_PASSWORD} \
    --account-mail=${ADMIN_ACCOUNT_EMAIL}
