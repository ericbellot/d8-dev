#!/bin/bash

# shellcheck disable=SC2059
mkdir -p "${PATH_PUBLIC_HTTP}/modules/custom" "${PATH_PUBLIC_HTTP}/themes/custom"
php-doc-check \
  -$ [module, inc]  \
  --complexity-error-threshold 1 \
  "${PATH_PUBLIC_HTTP}/modules/contrib" \
  "${PATH_PUBLIC_HTTP}/themes/custom"

#  --file-extension ctp, engine, hphp, inc, install, module, php, php4, php5, phtml, profile, test, theme \
