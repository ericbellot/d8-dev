'use strict';

const autoprefixer = require("autoprefixer");
const del = require("del");
const fs = require('fs');
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const sass = require("gulp-sass");
const yaml = require('js-yaml');

// Get options.
const options = yaml.safeLoad(fs.readFileSync('./settings/gulp-options.yml', 'utf8'));
var sassPaths = [];
var cssPaths = [];
if(Boolean(options.themes_paths) && Array.isArray(options.themes_paths)) {
  for (var themePath of options.themes_paths) {
    if(!Boolean(themePath)) {
      continue;
    }
    sassPaths.push(themePath + "/sass/**/*.scss");
    cssPaths.push(themePath + "/css/");
  }
}
if(sassPaths.length === 0) {
  throw Error('Gulp error: option "themes_paths" not set correctly in ./settings/gulp-options.yml.');
}
const outputStyles = [
  'nested',
  'expanded',
  'compact',
  'compressed'
];
if(!Boolean(options.css_output_style) || !outputStyles.includes(options.css_output_style)) {
  throw Error('Gulp error: option "css_output_style" not set correctly in ./settings/gulp-options.yml.');
}
const cssOutputStyle = options.css_output_style;

// Task: Clean up of generated assets.
function clean() {
 return del(cssPaths);
}

// Task: Convert Sass files to CSS.
function css(cb) {
  for (var themePath of options.themes_paths) {
    // console.log(themePath);
    gulp.src(themePath + '/sass/**/*.scss')
      // Plumber avoid crash when error occur during watching process.
      .pipe(plumber())
      // Make Sass convernsion.
      .pipe(sass({debugInfo   : true, outputStyle: cssOutputStyle}).on('error', sass.logError))
      // Supported browsers are defined in .browserslistrc.
      .pipe(postcss([autoprefixer()]))
      .pipe(gulp.dest(themePath + '/css'));
  }
  // Callback function added to signal async completion.
  cb();
}

// Task: Watch Sass files and make CSS conversion when file is changed.
function watchCss() {
  return gulp.watch(sassPaths, css);
}

// Make series of tasks which are processed with same Gulp command.
const defaultTasks = gulp.series(clean, gulp.parallel(css));
const watchTasks = gulp.series(gulp.parallel(watchCss));

// Export tasks usable by Gulp in command line:
// - `gulp css`: convert Sass files to CSS.
// - `gulp clean`: delete all generated CSS files.
// - `gulp`: without argument process defaultTasks.
// - `gulp watch`: watch Sass files and make CSS conversion when file is changed.
exports.css = css;
exports.clean = clean;
exports.default = defaultTasks;
exports.watch = watchTasks;
