#!/bin/bash

# shellcheck disable=SC2059
# Use PHP Copy Paste Detector to found duplicated code.
phpcpd \
   --progress \
   --names=*.ctp,*.engine,*.hphp,*.inc,*.install,*.module,*.php,*.php4,*.php5,*.phtml,*.profile,*.test,*.theme \
   --min-lines 10 \
   -- \
  "${PATH_PUBLIC_HTTP}/core" \
  "${PATH_PUBLIC_HTTP}/modules" \
  "${PATH_PUBLIC_HTTP}/themes"

