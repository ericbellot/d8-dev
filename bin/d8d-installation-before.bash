#!/bin/bash

# shellcheck disable=SC2059

echo "Generate Drupal settings files."
cp -n "${PATH_SITES}/default/default-d8dev.settings.php" "${PATH_SITES}/default/settings.php"
cp -n "${PATH_SITES}/default/default-d8dev.settings.local.php" "${PATH_SITES}/default/settings.local.php"


if [ "${HOST_SYSTEM}" = "linux" ]; then
  echo "Set permissions on files."
  sudo chown -vR "${USER_UID}:www-data" ${PATH_PROJECT}
  sudo chmod -vR 2775 ${PATH_PROJECT}
fi
